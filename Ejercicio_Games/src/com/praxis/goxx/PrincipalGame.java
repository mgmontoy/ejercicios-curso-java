package com.praxis.goxx;

public class PrincipalGame {
	
	public interface Playable {

		void walk(double x, double y);
		default void setGameName(String name) 
	    { 
	      System.out.println("Argumento: "+ name); 
	    }
	}
	
	public interface Soundable {
		
		void playMusic(String song);
		default void setGameName(String name) 
	    { 
	      System.out.println("Argumento: "+ name); 
	    }
	}
	
	public interface Gameable {

		void startGame();
		default void setGameName(String name) 
	    { 
	      System.out.println("Argumento: "+ name); 
	    }
	}

	private static String gameName;
	private static double playerPosx = 0.0;
	private static double playerPosy = 0.0 ;
	
	public static void main(String[] args) {
		Playable p = (x,y) -> {
			playerPosx++;
			playerPosy++;
			System.out.println("playerPosx["+ playerPosx +"]"+ " playerPosy[" + playerPosy+ "]");
		};
		p.walk(playerPosx,playerPosy);

	}

}
